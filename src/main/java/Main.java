import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import lombok.extern.slf4j.Slf4j;
import models.Input;
import models.Output;

import static spark.Spark.*;

@Slf4j
public class Main {
    private static final int port = 7845;

    public static void main(String[] args) {
        //declare port
        port(port);

        //first api for introduction
        get("/", (req, res) -> "Welcome to NhanVT's api. To make sum calculation, please call POST api path " +
                "\"/calc-sum\" with input format { \"first\": num1, \"second\" : num2 }.");

        //api for sum calculation
        post("/calc-sum", (req, res) -> {
            try {
                log.info(req.body());
                Gson gson = new Gson();
                Input input = gson.fromJson(req.body(), Input.class);
                if (input == null)
                    return "input can not be null or maybe your object key is not correct, please check again!!";
                else if (input.getFirst() == null)
                    return "first can not be null or maybe your object key is not correct, please check again!!";
                else if (input.getSecond() == null)
                    return "second can not be null or maybe your object key is not correct, please check again!!";
                else
                    return gson.toJson(new Output(input.getFirst() + input.getSecond()));
            } catch (JsonParseException e) {
                log.error(e.getMessage());
                return "Data format is wrong, input must be this format: { \"first\": num1, \"second\" : num2 }";
            }
        });
    }
}
